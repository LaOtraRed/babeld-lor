#!/bin/bash

# Este script prepara las variables de entorno para la compulacion cruzada
# de babeld para la arquitecutra MIPS 
# Se utiliza el sdk del proyecto lede.
# Se asume que el sdk esta dos directorios antes de este

# Tambien se puede usar para compilar:
# 
# ./set-up.sh build
# ./set-up.sh clean

# para copiarlo directamente al enrutador con <IP> dada (lo copia en /tmp/babeld
#
# ./set-up.sh export <IP>


OP1=$1
OP2=$2

ayuda()
{
    echo "Uso:"
    echo "Construye usando el cross compiler gcc mips-openwrt-musl-gcc:"
    echo "  ./set-up.sh build"
    echo "Equivalente a 'make clean':"
    echo "  ./set-up.sh clean"
    echo "Equivalente a 'make distclean':"
    echo "  ./set-up.sh distclean"
    echo "Construye y copia al enrutador con IP (usa root@IP y copia en /tmp/):"
    echo "  ./set-up.sh export IP"
    echo "Ejemplo:"
    echo "  ./set-up.sh export 192.168.1.1"
    echo ""
    echo "NOTA Antes de ejecutar:"
    echo "  La ruta hacia el SDK de LEDE debe estar en este mismo directorio"
    echo "  puede ser con un enlace simbolico con el nombre 'SDK'"
    echo "    ln --symbolic /ruta/absoluta/hacia/el/sdk/ SDK"
}

#### 
# setup inicial
setup()
{
    # agregando al path
    if ! echo $PATH | grep "mips-openwrt-linux-gcc-5.4.0/bin" 
    then
	RUTA_SDK=$(readlink SDK)
	PATH=$PATH:"$RUTA_SDK"staging_dir/toolchain-mips_24kc_gcc-5.4.0_musl/bin/
	export PATH
    fi

    # agregando staging dir
    if ! echo $STAGING_DIR | grep "lede-sdk-ar71xx-generic_gcc-5.4.0_musl.Linux-i686/staging_dir"
    then
	RUTA_SDK=$(readlink SDK)
	STAGING_DIR="$RUTA_SDK"staging_dir/toolchain-mips_24kc_gcc-5.4.0_musl/bin/
	export STAGING_DIR
    fi
    echo
    echo "PATH:$PATH"
    echo "STAGING_DIR:$STAGING_DIR"
}
####

build()
{
    echo "Construyendo..."
    setup
    echo
    echo "CROSS COMPILER:"
    mips-openwrt-linux-musl-gcc --version
    make CC=mips-openwrt-linux-musl-gcc PLATFORM_DEFINES='-march=mips32'
    file babeld
    echo "Hecho"
}

clean()
{
    echo "Limpiando..."
    setup
    make clean
}

distclean()
{
    echo "Limpiando todo..."
    setup
    make distclean
}

exportar()
{
    echo "Exportando..."
    build
    
    # copiando los tokens de autenticacion correctos para el prefijo dado
    echo "copiando tokens y clave publica.."
    rm tokens/ciphered-tokens.ctxt rsa_pub.txt 2> /dev/null
    cp pruebas/tokens/$OP2.ctxt tokens/ciphered-tokens.ctxt
    cp pruebas/tokens/rsa_pub.txt rsa_pub.txt

    scp -r babeld tokens/ rsa_pub.txt root@$OP2:/tmp/
    #scp -r babeld rsa_pub.txt tokens/ root@$OP2:/tmp/
    # scp -r babeld rsa_pub.txt tokens/ decrypt-lib/libmbed* \
    # 	decrypt-lib/mbedtls/ root@$OP2:/tmp/

}

####
# opciones
if [ -z "$OP1" ]
then
    ayuda
    exit 2
fi

if [ $OP1 == "build" ]
then
   build
   exit 0
fi

if [ $OP1 == "clean" ]
then
    clean
    exit 0
fi

if [ $OP1 == "export" ]
then
    R=$(echo $OP2 | grep "[a-z,A-Z]")
    if [ -z $OP2 ] || [ $R != "" ]
    then
	ayuda
	exit 2
    else
	exportar
	exit 0
    fi
fi

if [ $OP1 == "distclean" ]
then
    distclean
    exit 0
fi

ayuda
exit 2
####



# make CC=mipsel-linux-gcc PLATFORM_DEFINES='-march=mips32'
