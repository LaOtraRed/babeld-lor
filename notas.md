# Importantes encontrados en el codigo de babeld #

## Envio de mensajes ##

* **parse_packet**: en `message.c` parece ser la funcion que analiza los mensajes recibidos.
* **send_update**: en `message.c` es la funcion que *alista* los updates.
* **send_update_resend**: en `message.c` una funcion interesante similar a send_update un comentario (encontrado en xroute.c) dice lo siquitete:
       /* send_update_resend only records the prefix, so the update
       will only be sent after we perform all of the changes. */

* **really_send_update**: en `message.c` parece ser la funciona que prepara todos los datos de una interfaz de acuerdo a los parametros pasados y programa los tiempos de envio de los mensajes babel relacionados al update. Esta funcion ademas usa `start_message` y `end_message` para poner en los buffers datos sobre los TLV a ser enviados.

* **send_self_update**: en `message.c` parece una funcion que se encarga del *xroute_stream* y vuelve a llamar a `send_update`

* **flushupdates**: en `message.c`parece procesar los *buffered_updates* de una interfaz (struct interface), **quitarlos** (dejan de ser buffered_updates) y llamar a `really_send_update` o `schedule_update`. --> segun un comentario en el codigo:
            /* Since flushupdates only deals with non-wildcard interfaces */
* **flushbuf**: en `message.c` parece ser la funcion que ejecuta datos guardados en los buffers y usa `babel_send` en `net.c` para enviar los mensajes usando `sendmsg` (propia del kernel linux) para **enviar los mensajes al socket dado**.

* **buffer_update**: en `message.c` Agrega a `buffered_updates` la interfaz *ifp* dada el update con los argumentos dados. 

* **start_message**: en `message.c` usa `flushbuf` y despues marca `type` y `len` de la interfaz dada.
* **end_message**: en `message.c` usa `schedule_flush` que modifica el timeout del que el paquete usando un jitter.

* **handle_request**: en `message.c` se llama al recibirse un *MESSAGE_MH_REQUEST_SRC_SPECIFIC* o *MESSAGE_MH_REQUEST* (seqno request) y se encarga de verificar el *request* dado, por ejemplo actualizando el *seqno* de ser necesario y llamando a `send_update` en modo urgente.

## Gestion de rutas y fuentes ##

* **update_route**: en `route.c` es la funcion que es llamada cada que se recibe un update, al parecer modifica la tabla de rutas con los argumentos pasados y llama a `insert_route` de `route.c` que introduce una nueva ruta en la tabla o devuelve error para que desde otra funcion se libere la memoria que ocupa la ruta recibida.

* **check_xroutes**: en `xroute.h` parece ser la funcion que *comprueba* la metrica y filtros de rutas, las compara con la tabla de rutas (**babel_routes) las agrega o quita de la tabla de rutas y xroutes. Al parecer aqui se debe agregar cipher y clen propio cuando se trata de una ruta propia (metric==0).

* **find_route_slot**: En `route.c`, la funcion que busca en la tabla de rutas (babel_route **routes) la ruta con el prefijo de red dado y retorna el indice si la encuentra o -1 si no la encuentra.

* **find_installed_route**: en `route.c` usa `find_route_slot` en `route.c` para buscar la que una ruta dado el prefijo de red este en la tabla de rutas y este **instalada**.

* **consider_route**: en `route.c` decide si una ruta (babel_route) se va a instalar o no (**instalar rutas es distinto que agregar rutas**), busca tambien en *xroutes* si existe esta entrada. Si se decide instalar la ruta usa `switch_routes` y tambien llama  `send_update` o `send_update` segun el caso.

* **insert_route**: en `route.c` introduce una ruta en la talba de rutas, la funcion no verifica condiciones como factibilidad o bucles de enrutamiento solamente introduce la nueva ruta. Aqui al parecer no hay necesidad de verificar si existe token de autenticacion,

* **switch_routes**: en `route.c` Usa `install_route` o `uninstall_route` segun la necesidad, para esto hace uso de `move_installed_route` en `route.c`. Opera cambiando una vieja ruta por una nueva.

* **install_route**: En `route.c` introduce la ruta (babel_route) dada y la marca como instalada. Llama a `kinstall_route` de `disambiguation.c` para introducir una ruta en el kernel, si esto tiene exito usa `move_installed_route`.

* **uninstall_route**: En `route.c` marca como no instalada una ruta dada, usa `kuninstall_route` de `disambiguation.c` que quita la ruta instalada en el kernel.

**NOTA**: Para identificar a rutas "instaladas" de "insertadas" se verifica la variable *installed*.

## Comprobaciones ##

* **check_interfaces**: en `interface.c` una funcion que es usada desde el programa principal en `babeld.c` que comprueba todas las interfaces y usa funciones de bajo nivel como `kernel_interface_operational` en `kernel_netlink.c` para comprobar cambios de estado en las interfaces. Esta funcion tambien llama a `send_request` `send_update` al detectar cambios al parecer.

* **statisfy_request**: en `resend.c` parece una funcion que comprueba los *resends* (que llama *requests*) dada la intefaz, prefijo, id , seqno y otros dados  y los marca para ser reenviados o expirados.

## Estructuras a modificar ##

* struct **interface** en `inteface.h` una estructura que contiene informacion de la interfaz y buffers como hellos, nh y **updates**, ademas de intervalos de tiempo y rtt.

* `source.h`,  `source.c` manejan la tabla de fuentes (source table)
* `route.h`, `route.c` manejan la tabla de rutas (route table) no parece ser necesario modificar esta estructura por que contiene como miembro a `source`.

Es necesario modificar `source.h` y agregar funciones para actualizar `clen` y `cipher` a cada ruta o fuente, ademas de comprobar esta informacion, `buffered_update` en `inteface.h` es importante revisar, aunque se puede optar por modificar ciertas funciones y agregar `clen` y `cipher` a cada update o fuente.

* `xroute.h` Al parecer xroute es una estructura similar a `route.h` pero que meneja solamente datos relacionados a prefijo, metrica, , protocolo e indice de interfaz, no maneja nada de datos de rutas o interfaz, **parece estar destinada a almacenar rutas exportadas (exported routes)**. Ahi he introducido *clen* y *cipher*. Esta estructura parece tener la finalidad de simplificar la busqueda de rutas en una tabla aparte que ocuparia menos memoria y que no se trata necesariamente de rutas instaladas o usadas por el nodo.

* `resend.h` Ya que desde `do_resend` que es llamada desde el programa principal y en esta se usa `send_update` al parecer es necesario introducir tambien *clen* y *cipher* para que se envie tambien esta informacion en los *updates* reenviados.

**NOTA sobre la estructura interface**: Esta estructura contiene informacion relevante para una interfaz de red fisica o logica, no contiene informacion como rutas o fuentes `route` o `source` directamente pero *si se guardan buffers con informacion como id, nh y prefix* a parte de campos para esto, tambien la estructura `buffered_updates` de `interface.h`.

Los datos en los buffers al parecer se utilizan para acumular datos para ser enviados despues con `babel_send` en `net.c` o `start_message`. Los datos se acumulan en los buffers con las funciones `accumulate_byte` `accumulate_bytes`, `accumulate_short` y `accumulate_int` de `message.c`.

## Archivo de configuracion ##

En `configuration.c` hay funciones para ajustar la configuracion incluyendo leer archivos.

* `parse_config_from_string`
* `parse_config_from_file`
* `parse_config_line`

## Cifrar mensajes ##

El siguiente pedazo de codigo funciona llamandose desde babeld.c

    // -- quick rsa decrypt tests --
      printf("\n---- testing decrypt module ----\n");
      printf("rsa_init: %d \n" , rsa_module_init("rsa_pub.txt",
    						   &rsa_context,
    						   &rsa_entropy,
    						   &rsa_ctr_drbg));
      printf("rsa_context:");
      printf(" N: %d \n E: %d\n", rsa_context.N, rsa_context.E);
      printf(" len: %d \n", rsa_context.len);
    
      /* char *encrypted_message = "85E986F19D9678A03C23435B7A27B455ABCFF0C452057FF2F851E9269BCF4DE014199E99EFB05F9E7302BB4E5E07D976C5B417F1B46ACB53F080EA4EB82F137418A27CB9954243C15FB2DE896C7ACA40CA551DF5CA18A31EB279184ABA312111C44C88819BEE968786CB3F469A98EEC574668F750F188E62795D9688229E6CBF7E8292C517F2C3AF7D121321D9317B4B8D1D6EFCE3F232E7A20E34AFF65A1F7E0D0040C9F90C46A772CB2E06337D356B3E669981C194E9AFEE4B885C854984DDAF72E9B5A881FF9E242A6EAC33022E0DEF5B3F0109F368E1B2B76C6AF35697E2332CE5DD1A1B29E35473814B8204070724DEA10B06BC5C491078A88667E826DF"; */
      char *encrypted_message = "6041837C0401C36691081C6DACEA2BB746A1E9E368AA00F3D6C26CFEEB2AEA5666A151C0628873DF9CE04CB4706EB5F28CA8DFE1F340FC1CF7A09653065F41FD4246ACEC9677F603FE5A0115036C61FCD526B98CF08EA502405E86949C2B208CB8EE4326C97741D9F86821924F8CC6C11ECD7E3F33D87208E56EED8E449E7245BD5A69149696ED74693F0D3A429DCF87BC5788392262720383160484FC812046478E13BE6A180472D418064E49FA90783AB87FDB491C0029D6CF6E7CCB9B1AA3B448DD201D074EB5B288950B042EA4C6E7FB33E26E7BC28BCE9F73757DBA3C6B78EF5702FA16309D1797981B1EAF576D3E6CA9BC61E1624E3F2436A2CA0D0F55";
    
    
      printf("rsa_decrypt: %d \n", rsa_decrypt(&rsa_context,
    					 &rsa_entropy,
    					 &rsa_ctr_drbg,
    					 encrypted_message,
    					 &rsa_result));
      printf("decrypted message: %s\n", rsa_result);
    
      printf("rsa_decrypt: %d \n", rsa_decrypt(&rsa_context,
    					 &rsa_entropy,
    					 &rsa_ctr_drbg,
    					 "293FBB480F423A43FAA0F794B54682EB9593E81CC286E8135AF4F135E2275C9B09DFF5FDAD6052876E47A998AFF56318F0986E5467BE26C7ACECF8799825D93EB03BE9BC41B86AE66A99551FDAC66D10821AA5FE27391A534846297378A75D7386EAA285EB4A18CB1F0CC2B3BA5903D0E7166DCD9BE4BFEEEEB820F0524C492748084535C5D28500D77382437BEFAA1AD8347913D2F71D7EC725B61B6FAABDB4C1EA77BAEBDE37ED997990E49193A9FDAC772A0EAFFB735B128C593D15953AD75A310BDB95AAACF39229DF3634422DB3F97FDA095F21982F73AD3723844AC59E93EA0FC7C16EFD6723D2385A476818B5A2AC9363F59B13C3D4DFBB82B2AC0676",
    					 &rsa_result));
      printf("decrypted message: %s\n", rsa_result);
      
      printf("\n-----------\n");
      // ----

## plan para extraer cadenas de cifrado ##

En el archivo `ciphered-tokens.txt` se guardan los 100 tokens de autenticacion generados por la entidad **C** se supone que estan guardados en formato sin espacios en blanco.

En el archivo `decrypt.c` deberia introducir funciones para extraer la cadena cifrada correspondiente al `seqno` y `router_id` como se especifica en la documentacion de autenticacion.

## notas de edicion de codigo ##

Estoy modificando cada funcion que tenga que ver con actualizacion de rutas y agregando como argumentos `cipher` y `clen`.

* Actualmente estoy editando la funcion `update_route` de `route.c`.
  * Eso me ha llevado a editar `parse_packet` en `message.c`
Estoy dejando pendiente la edicion de `update_route` y `parse_packet` por que necesito trabajar en enviar el paquete adecuadamente.

* Estoy modificando la funcion `send_update` en `message.c` para ver como deberian enviarse los paquetes para agregar clen y cipher.
  * Al parecer la estructura `xroute_stream` es una estructura para contener indices de la estructura `xroute`, no entiendo aun por que se necesita crear una estructura con un solo atributo entero com `xroute_stream` y no manjearlo solo en una variable indice, aunque posiblemente para alamcenar los indices que pueden ir cambiando no necesariamente linealmente.
  * Segun parece los `route_stream` se usan temporalmente.
  * Tengo que revisar que es lo que hace la funcion `handle_request` ya que esta llama a `send_update`
  * He encontrado la estructura `resend` en `resend.h` que es tambien una lista enlazada. Al parecer esta estructura es para almacenar *resends* o reenvios de updates por que la funcion `do_resend` en `resend.c` usa `send_update`, asi que seguramente tendre que agregar aqui tambien los campos `clen` y `cipher`.

* Ahora estoy buscando desde donde se ordena en primera instancia el envio de un update desde una **direccion propia** (metric=0) ahi tengo que llenar *cipher* y *clen* para que se anuncie el token propio de este router en el siguiente update. Haciendo prints he visto que `check_interfaces` en `intefaces.c` no comprueba rutas o calcula metricas, debo centrar el esfuerzo para llenar *cipher* y *clen*, seguramente en `check_xroutes` en `xroute.c`.

** NOTA ** En la funcion `check_xroutes` se llama a `add_xroute` y esa usa `kernel_route` para extraer datos y ponerlos en un nuevo xroute. Como modificar `kernel_route` conllevara a modificar mucho codigo de bajo nivel que no pueda tener tiempo de comprender, extoy extrayendo cipher, clen de la estructura `xroute` usando la funcion `find_xroute` en `xroute.c`, no estoy seguro si es lo correcto.

* He modificado `send_update` y `buffer_update` para hacer que antes de un update se envien *clen* y *cipher* si existen para ese prefijo, y tambien he modificado `really_send_update` agregando al buffer de una interfaz *clen* y *cipher* esto ha ocasionado que este nuevo TLV se envie y he tenido que ignorar el assert de `end_message` ya que la longitud del TLV LORAUTH es de 514 (mas de un byte). Queda hacer pruebas en la recepcion de paquetes *estando atento al type=29* para tomar 2 bytes como longitud del TLV y continuar para interpretar este mensaje.

* Modificando `send_update` y `buffer_update` para hacer que en cada update se envie *clen* y *cipher*, al menos de las rutas que tengan estos datos. De `buffer_update` ha surgido la necesidad de modificar `really_send_update`, despues de modificar `really_send_update` he conseguido que se envie el TLV lorauth con *cipher* y *clen* pero en tres paquetes para respetar las especificaciones de babel (RFC 6126).

* Ahora me toca editar nuevamente `parse_packet` en `message.c` y verificar el token de autenticacion, que este de acuerdo con el prefijo anunciado y que se anuncien nuevamente las rutas aceptadas y autenticadas.
 * De eso ha surgido el problema de que `rsa_decrypt` en `decrypt.c` no logra descifrar el token recibido (quizas por que lo envio como char[] en lugar de char*?), debo poder corregir ese problema para poder autenticar los updates.

* He conseguido hacer que se descifren los tokens enviados y que se envien correctamente, ahora tengo que modificar la comprobacion del token recibido ya que al anunciar la ruta 80.0.2.0/24 babeld registra y envia  el prefijo del *update* asi: `0 0 0 0 0 0 0 0 0 0 255 255 80 0 2 0`, y no contiene `80.0.2.1` que es la direccion del router que hace el anuncio (solamente el prefijo que anuncia). Por eso el script que crea los tokens de autenticacion debe tener esto en cuenta y **crear los tokens usando el prefijo que el nodo anunciara**, no la direccion propia del router. -- El problema es que la mascara o CIDR no se contempla en el mensaje cifrado, seguramente tendre que modificar la especificacion para que este contenga el CIDR tambien. Deberia averiguar --> como babeld realmente empotra los CIDR del prefijo anunciado y de alguna manera incluirlo en el mensaje *cipher* para que se haga la comprobacion. Por el momento solamente hare que se compruebe el prefijo e ignorare el CIDR no incluyendolo en la comprobacion del token *cipher*.

* Ahora estoy modificando `handle_request` en `message.c` para hacer que luego de recibir un *seqno request* se envie un update (llamando a `send_update`) usando clen y cipher correspondiente a la ruta instalada. **revisar los pedazos de codigo que dicen**:

/* lorauth note: .. */

* En hacer que se envien rutas con el token de autenticacion aceptado de otro nodo, debo modificar las funciones de `resend.c`.

## BUGS registrados ##

### desinstalar rutas ###

En la nueva modificacion al quitar una ruta se da el error:

    uninstall_route(80.0.2.0/24 from ::/0)
    
    
    Sending update to wlan0 for 80.0.2.0/24 from ::/0 
    cipher: 098..DB
     done buffering clen, cipher (512 bytes)
      (flushing 4 buffered updates on wlan0 (5))
      -- find_xroute(): numxroutes:1
         prefix:80.0.2.0/24, src_prefix:::/0
    	 xroutes[0].prefix: 80.0.3.0/24
    	 xroutes[0].cipher: 914..DE
    
    
    
    Segmentation fault

Debo comprobar si se tratan de enviar updates aun con el campo *cipher* en NULL.


### quitar rutas instaladas (solucionado) ###

Cuando un nodo autenticado envia una retraccion (Update_route_metric=256 ?) se produce una violacion de segmento.

    --Update_route_metric: 256
    
    My id 88:9e:93:95:7b:b2:1c:73 seqno 2028
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 0001 rxcost 65535 txcost 259 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher 747C...7C (exported)
    80.0.1.0/24 metric 65535 (65535) refmetric 0 id f4:c2:12:45:e4:3f:bf:30 seqno 5968 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 07A..17
    Checking neighbours.
    uninstall_route(80.0.1.0/24 from ::/0)
    
    My id 88:9e:93:95:7b:b2:1c:73 seqno 2028
    80.0.2.0/24 metric 0 cipher 747C...7C (exported)
    
    My id 88:9e:93:95:7b:b2:1c:73 seqno 2028
    80.0.2.0/24 metric 0 cipher 747C...7C (exported)
    Segmentation fault

Al perecer al quitar una ruta instalada se esta produciendo el error.

* El error ya no se presenta por lo menos en los dos minutos que lo he probado, problablemente por que estaba tratando de enviar updates con *cipher* como NULL o estaba quitando las rutas adecuadamente.

### Reenvio de updates con cipher recibido ###

Una vez que se instalan las rutas y se hace la autenticacion por primera vez, cuando un nodo trata de reenviar el *update* de otro parece no estar usando la cadena cipher que ha recibido previamente, y es como si buscase en su propio conjunto de tokens y pone en el TLV-LORAUTH un *cipher* suyo, por ejemplo:

    --- lorauth AUTHENTICATION ---
     --- lorauth AUTHENTICATION ---
    	prefix not found in decrypted token (contiguous check)
    	rsa_result: 80.0.1.0_40411679 
    prefix: 0 0 0 0 0 0 0 0 0 0 255 255 80 0 2 0 
    		*** FAILED ***

Los nodo 80.0.1.0 y 80.0.2.1 ya se han autenticado mutuamente, luego el nodo 80.0.1.0 esta tratando de esparcir la ruta hacia 80.0.2.0 para otros nodos. Pero no esta usando el token *cipher* recibido de 80.0.2.1 en primera instancia y pone un *cipher* propio. Por eso la verificacion falla y este problema podria causar otros quizas bulces de enrutamiento o "hambre falsa".

### Error al paresear TLV lorauth (solucionado) ###

He modificado la funcion `parse_packet` de `message.c` para reconocer este TLV y comenzar a interpretarlo, pero por alguna razon en un router TLMR3020 al ejecutar el codigo siempre el buffer parece duplicarse:

    80.0.1.0/24 metric 0 cipher 5311...83 (exported)
      (flushing 6 buffered updates on wlan0 (5))
      --Done sent lorauth TLV.
      --Done sent lorauth TLV.
      (flushing 1114 buffered bytes on wlan0)

En otro mensaje :

    (flushing 8 buffered bytes on wlan0)

**RAZON**: Una inadecuada configuracion en `/etc/config/babeld` causaba que las rutas no se exporten correctamente y nunca se instalen, una vez corregido este problema en ambos routers los buffers **ya no se duplican** y se envia de la forma:

    My id d4:a4:3d:06:cf:3d:ba:fa seqno 23269
    80.0.1.0/24 metric 0 cipher 580F...8D (exported)
      (flushing 2 buffered updates on wlan0 (14))
      --Done sent lorauth TLV.
      (flushing 567 buffered bytes on wlan0)

----
Sin embargo el error *segmentation fault* ocurre al tratar de actualizar una ruta:

    Received update for 80.0.1.0/24 from fe80::ea94:f6ff:fed9:fc10 on wlan0.
    --trying update_route
     --Cipher on update
         buffered_cipher:58...8D size: 512
    -->update_route id:d4:a4:3d:06:cf:3d:ba:fa
     prefix:00:00:00:00:00:00:00:00 plen:120
     seqno:23269 refmetric: 0
     cipher: 58...8D clen:512
     myid:50:3f:26:3b:e2:ef:79:e1
    Segmentation fault

Tengo que encontrar la fuente del *segmentation fault*  que se al llamar a `update_route` de `route.c`

**PRELIMINAR** He hecho algunos ajustes en `lorauth.c` y revisando las advetencias que pone el compilador, al parecer no estoy haciendo correctamente el manejo de cadenas por que eso esta provocando *memory leaks* o acceso a sectores incorrectos.

Ahora el *segmentation fault* se ha cambiado y no se dispara en `uptade_route`

    My id 1c:cd:dd:91:78:66:61:62 seqno 8974
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach fc00 rxcost 264 txcost 260 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher ... (exported)
    80.0.1.0/24 metric 268 (32767) refmetric 0 id f0:a7:e3:e4:5b:8a:42:1e seqno 54318 age 0 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 
      (flushing 1 buffered updates on wlan0 (5))
    Segmentation fault

Tengo que seguir corrigiendo errores y revisando warnings.

He limpiado casi todos los warnings y modificado algo del codigo, el *segmentation fault* parece haberse movido cuando edite `find_source`, `find_source_slot` en `source.c`.

    My id 0c:d7:d2:78:9b:73:13:fe seqno 47721
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffc0 rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher 4BF9...13 (exported)
    80.0.1.0/24 metric 256 (32767) refmetric 0 id f0:43:10:d3:75:f9:4e:bb seqno 57208 age 0 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 1B...AF
      (flushing 3 buffered updates on wlan0 (5))
      --Done sent lorauth TLV.
    Segmentation fault

Estoy pensando que hay problemas al probar cipher, clen en estas dos funciones o probablemente en `send_update` ya que dice que se envian 3 updates? tengo que revisar eso tambien.

**SOLUCION 1** En la funcion `flushupdates` en `message.c` al llamar a `really_send_update` se trataba de extraer de `xroute->cipher` la cadena cifrada pero esta estaba vacia y por eso se generaba la violacion de segmento, en lugar de eso envio NULL y `clen=0`

No se deteiene el programa pero hay un error al instalar la ruta:

    My id 88:ad:b8:f9:0a:1e:eb:59 seqno 6676
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 8000 rxcost 1023 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher 2E75...8D (exported)
    80.0.1.0/24 metric 65535 (65535) refmetric 0 id b8:3e:df:a2:95:49:fc:c2 seqno 34451 age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 30...00
      (flushing 24 buffered bytes on wlan0)

Ahi se ve que se instala la ruta `80.0.1.0/24` con metrica 65535 que `INFINITE` y es una ruta no valida, mas bien una retraccion.

Debo revisar como agregar rutas y *xroutes* correctamente previamente haciendo la verificacion del token *cipher*.

**NOTA** Despues de un tiempo los routers comienzan a intercambiar rutas correctamente por ejemplo:

    My id 40:4d:13:a2:88:3d:08:7f seqno 49282
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 8000 rxcost 1023 txcost 1019 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher 3D48...9C (exported)
    80.0.1.0/24 metric 4072 (3896) refmetric 0 id 14:43:1a:c0:38:a0:a7:04 seqno 45626 age 1 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 44...08
    Sending self update to wlan0.
    Sending update to wlan0 for 80.0.2.0/24 from ::/0 
    cipher: 3D...9C
     done buffering clen, cipher (512 bytes)
    Sending update to wlan0 for any.
     done buffering clen, cipher (512 bytes)
    Sending self update to wlan0.
    Sending update to wlan0 for 80.0.2.0/24 from ::/0 
    cipher: 3D...9C
     done buffering clen, cipher (512 bytes)
    Sending update to wlan0 for any.

Pero se produce nuevamente *segmentation fault* casi simultaneamente por ejemplo en el router `80.0.3.1`:

    My id 14:43:1a:c0:38:a0:a7:04 seqno 45626
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 4401 rxcost 910 txcost 1023 rtt 0.000 rttcost 0 chan 7.
    80.0.1.0/24 metric 0 cipher 4401...08 (exported)
    80.0.2.0/24 metric 65535 (65535) refmetric 65535 id 40:4d:13:a2:88:3d:08:7f seqno 49283 age 106 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    cipher: 3D...9C
      (flushing 1 buffered updates on wlan0 (5))
      -- find_xroute(): numxroutes:1
         prefix:80.0.2.0/24, src_prefix:::/0
        xroutes[i].prefix: 80.0.1.0/24
        xroutes[i].cipher: 44...08
      (flushing 59 buffered bytes on wlan0)
    
    My id 14:43:1a:c0:38:a0:a7:04 seqno 45626
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 4401 rxcost 910 txcost 1023 rtt 0.000 rttcost 0 chan 7.
    80.0.1.0/24 metric 0 cipher 4401...08 (exported)
    80.0.2.0/24 metric 65535 (65535) refmetric 65535 id 40:4d:13:a2:88:3d:08:7f seqno 49283 age 106 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    cipher: 3D...9C
    
    My id 14:43:1a:c0:38:a0:a7:04 seqno 45626
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 4401 rxcost 910 txcost 1023 rtt 0.000 rttcost 0 chan 7.
    80.0.1.0/24 metric 0 cipher 4401...08 (exported)
    80.0.2.0/24 metric 65535 (65535) refmetric 65535 id 40:4d:13:a2:88:3d:08:7f seqno 49283 age 107 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    cipher: 3D...9C
    Segmentation fault

Y en el `80.0.2.1`:

    My id 40:4d:13:a2:88:3d:08:7f seqno 49282
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 8000 rxcost 1023 txcost 818 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher 3D48...9C (exported)
    80.0.1.0/24 metric 3269 (3630) refmetric 0 id 14:43:1a:c0:38:a0:a7:04 seqno 45626 age 6 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 44...08
    Received hello 25963 (400) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
    Sending hello 1062 (400) to wlan0.
    Sending ihu 1023 on wlan0 to fe80::ea94:f6ff:fed9:fc10.
    --Update_route_metric: 1363
    Received ihu 818 (1200) from fe80::ea94:f6ff:fed9:fc10 on wlan0 for fe80::ea94:f6ff:fe6b:80fa.
    
    My id 40:4d:13:a2:88:3d:08:7f seqno 49282
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 8000 rxcost 1023 txcost 818 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 cipher 3D48...9C (exported)
    80.0.1.0/24 metric 3269 (3561) refmetric 0 id 14:43:1a:c0:38:a0:a7:04 seqno 45626 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    cipher: 44...08
    Received nh 80.0.3.1 (1) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
    Received router-id a0:88:d7:c7:a4:ee:d7:b2 from fe80::ea94:f6ff:fed9:fc10 on wlan0.
    Received update for 80.0.2.0/24 from fe80::ea94:f6ff:fed9:fc10 on wlan0.
     len: 13
    --trying update_route
     --No Cipher on update --update_route id:a0:88:d7:c7:a4:ee:d7:b2
     prefix:80.0.2.0/24 plen:120
     seqno:41625 refmetric: 65535
     cipher: -NULL- clen:0
     myid:40:4d:13:a2:88:3d:08:7f
       ****No LORAUTH token, rejecting update***** 
    Received request (127) for 80.0.2.0/24 from fe80::ea94:f6ff:fed9:fc10 on wlan0 (40:4d:13:a2:88:3d:08:7f, 49283).
      -- find_xroute(): numxroutes:1
         prefix:80.0.2.0/24, src_prefix:::/0
        xroutes[i].prefix: 80.0.2.0/24
        xroutes[i].cipher: 3D...9C
    Segmentation fault

Probablemente la memoria se llena incorrectamente (revisar como se agregan y actualizan rutas), ademas no se tiene claro por que se siguen enviando updates sin el token LORAUTH.

**NOTA**: Es posible que el segmentation fault se de por que en la funcion consider_route se esta buscando un *xroute* que no existe, sin embargo aun se estan enviando *updates* sin el token LORAUTH.

**SOLUCION**: He corregido la funcion `reduced_lorauth_token` y quitado el *calloc* ya que no se estaba liberando el espacio de memoria y eso parecia estar contribuyendo al *segfault* que en casi 1 hora de prueba no se ha vuelto a manifestar y se ha intercambaiado e instalado rutas correctamente.

