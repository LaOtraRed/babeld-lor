#!/bin/bash

# NOTA: el script no se ha probado correctamente

# Este script prepara las variables de entorno para la compulacion cruzada
# de babeld para la arquitecutra RAMIPS
# Se utiliza el sdk del proyecto lede.
# Se asume que el sdk esta dos directorios antes de este

# Tambien se puede usar para compilar:
# 
# ./set-up.sh build
# ./set-up.sh clean

# para copiarlo directamente al enrutador con <IP> dada (lo copia en /tmp/babeld
#
# ./set-up.sh export <IP>


OP1=$1
OP2=$2

ayuda()
{
    echo "Uso:"
    echo "Construye usando el cross compiler gcc mips-openwrt-musl-gcc (RA-MIPS):"
    echo "  ./set-up.sh build"
    echo "Equivalente a 'make clean':"
    echo "  ./set-up.sh clean"
    echo "Construye y copia al enrutador con IP (usa root@IP y copia en /tmp/):"
    echo "  ./set-up.sh export IP"
    echo "Ejemplo:"
    echo "  ./set-up.sh export 192.168.1.1"
    echo ""
    echo "NOTA Antes de ejecutar:"
    echo "  La ruta hacia el SDK de LEDE debe estar en este mismo directorio"
    echo "  puede ser con un enlace simbolico con el nombre 'SDK'"
    echo "    ln --symbolic /ruta/absoluta/hacia/el/sdk/ SDK"
}

#### 
# setup inicial
setup()
{
    # agregando al path
    if ! echo $PATH | grep "toolchain-mipsel_24kc_gcc-5.4.0_musl-1.1.16"
    then
	RUTA_SDK=$(readlink SDK-ramips)
	PATH=$PATH:"$RUTA_SDK"staging_dir/toolchain-mipsel_24kc_gcc-5.4.0_musl-1.1.16/bin/
	export PATH
    fi

    # agregando staging dir
    if ! echo $STAGING_DIR | grep "lede-sdk-17.01.2-ramips-rt305x_gcc-5.4.0_musl-1.1.16.Linux-x86_64/staging_dir"
    then
	RUTA_SDK=$(readlink SDK-ramips)
	STAGING_DIR="$RUTA_SDK"staging_dir/toolchain-mipsel_24kc_gcc-5.4.0_musl-1.1.16/bin/
	export STAGING_DIR
    fi
    echo
    echo "PATH:$PATH"
    echo "STAGING_DIR:$STAGING_DIR"
}
####

build()
{
    echo "Construyendo..."
    setup
    echo
    echo "CROSS COMPILER:"
    mips-openwrt-linux-musl-gcc --version
    make CC=mipsel-openwrt-linux-musl-gcc PLATFORM_DEFINES='-march=mips32'
    #file babeld
    echo "Hecho"
}

clean()
{
    echo "Limpiando..."
    setup
    make clean
}

exportar()
{
    "Exportando..."
    build
    scp babeld root@$OP2:/tmp/
}

####
# opciones
if [ -z "$OP1" ]
then
    ayuda
    exit 2
fi

if [ $OP1 == "build" ]
then
   build
   exit 0
fi

if [ $OP1 == "clean" ]
then
    clean
    exit 0
fi

if [ $OP1 == "export" ]
then
    R=$(echo $OP2 | grep "[a-z,A-Z]")
    if [ -z $OP2 ] || [ $R != "" ]
    then
	ayuda
	exit 2
    else
	exportar
	exit 0
    fi
fi

ayuda
exit 2
####



# make CC=mipsel-linux-gcc PLATFORM_DEFINES='-march=mips32'
