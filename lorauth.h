/* 
   babeld-lor
    Copyright (C) 2017  Rodrigo Garcia

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define LORAUTH_TOKENS_DIR "tokens"
#define LORAUTH_TOKENS_FILE "ciphered-tokens.ctxt"

#define LORAUTH_CIPHER_LEN 512

//extern unsigned char mylorauth_cipher[LORAUTH_CIPHER_LEN+1];

int lorauth_token(unsigned char *dest, unsigned char id[8], 
		  unsigned short seqno);
int lorauth_token_index(unsigned char id[8], unsigned short seqno);

int check_lorauth_token(unsigned char id[8], 
			unsigned char prefix[16],
			unsigned short seqno,
			unsigned short clen,
			unsigned char *cipher);

const char* reduced_lorauth_token(const unsigned char *token);
void clean_cipher(unsigned char *buffered_cipher);
