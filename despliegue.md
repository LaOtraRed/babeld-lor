# sobre como la extension ejecutar en otros routers #

**TODO:** Arreglar el descifrado (mbedtls error -16440 = -0x4100) --> Ver solucion temporal en la siguiente seccion

## bibliotecas dinamicas y otros archivos ##

En el router copiando las bibliotecas .so en `/usr/lib` y luego haciendo un enlace sumbilico a por ejemplo `/tmp` deberia haber algo asi:

    lrwxrwxrwx    1 root     root            23 Jun  9 05:50 libmbedcrypto.so -> /tmp/libmbedcrypto.so.0
    lrwxrwxrwx    1 root     root            21 Jun  9 05:48 libmbedtls.so -> /tmp/libmbedtls.so.10
    lrwxrwxrwx    1 root     root            21 Jun  9 05:49 libmbedx509.so -> /tmp/libmbedx509.so.0

Tambien deberia funcionar si solo se instala el paquete `libmbedtls` (comprobar)

Se debe contar con el archivo de clave publica `rsa_pub.txt` en el mismo directorio donde esta el ejecutable de babeld, tambien la carpeta `tokens` con los tokens de cifrado.

### Bibliotecas construidas incorrectamente ###

Al parecer las bibliotecas dinamicas construidas dentro de `decrypt-lib` **no estan funcionando bien** o estan incorrectamente construidas.

He copiado al directorio `/tmp/vae` en el enrutador las bibliotecas `libmbedcrypto.so.0 libmbedtls.so.10 libmbedx509.so.0` ya que en `/usr/lib` he creado enlaces simbolicos como se muestra arriba. Ahora al ejecurar la version modificada de babeld funciona el modulo de descifrado RSA.

Ahora como se construyen usando bibliotecas dinamicas, he tenido que copiar `libmbedcrypto.so.0 libmbedtls.so.10 libmbedx509.so.0`y hacer enlaces simbolicos como se ve arriba **en la maquina donde construyo el codigo** (HOST del compilador cruzado).

## ejecutar ##

En el router 80.0.2.1 donde ya se habia instalado babeld y configurado se tiene el siguiente contenido en el archivo `/var/etc/babeld.conf`.

    random-id true
    ipv6-subtrees true
    interface wlan0
    interface wlan0 channel 7
     in ip 80.0.0.0/22  allow
     in ip fc01:1934::/32
     redistribute ip 80.0.2.1/24  allow
     in  deny
     redistribute local  deny

Luego se puede ejecutar con:

    ./babeld -c /var/etc/babeld.conf -d 2

Se puede comprobar que las bibliotecas dinamicas se enlazan correctamente al hacer

    ldd ./babeld
