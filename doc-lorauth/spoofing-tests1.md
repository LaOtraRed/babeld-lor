# Pruebas de suplantacion 1 #

Se tiene tres routers:

* **suplantado** que es el legitimo acreedor del prefijo `80.0.3.1/24`
* **suplantador** que ha copiado el prefijo `80.0.3.1/24` del suplantado.
* **enganyado** que recibe anuncios de ambos routers que claman ser acreedores de `80.0.3.1/24` y este decide con quien comunicarse.

El unico cambio en la configuracion de babel ha sido, hacer que el router suplantador anuncie el mismo prefijo que el router suplantado en este caso `80.0.3.1/24`.

## prueba de envio de paquetes a destinos incorrectos ##

En principio el router `enganyado` tiene una ruta hacia el `suplantado` (80.0.3.1) y luego se enciende el `suplantador` para intentar hacer que el router `enganyado` le envie paquestes al `suplantador` en lugar de al `suplantado`.

**Importante**: el router `suplantado` se coloca intencionalmeten mas lejos que el `suplantador` para que babel advierta una metrica mas baja y eventualemente prefiera la ruta hacia `80.0.3.1` a traves del `suplantador`.

### router enganyado (80.0.2.1) ###

El router `enganyado` envia paquetes ICMP a `80.0.3.1` y hace una pausa de 1 segudno al cabo de 8 paquetes mediante:

    A=0
    while [ $A -lt 500 ]
	do
	   ping -q -c 8 80.0.3.1
	   sleep 1
	   let "A=A+1"
    done

Se captura los paquetes ICMP recibidos en el router `suplantador` y `suplantado` con:

    tcpdump -i wlan0 | grep ICMP

### router suplantado ###

En principio recibe los paquetes del `enganyado` y *tcpdump* muestra:

    16:05:05.576040 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1662, seq 4, length 64
    16:05:06.610630 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1662, seq 5, length 64
    16:05:06.610780 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1662, seq 5, length 64
    16:05:07.567638 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1662, seq 6, length 64
    16:05:07.567781 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1662, seq 6, length 64
    16:05:08.568000 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1662, seq 7, length 64
    16:05:08.568145 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1662, seq 7, length 64
    16:05:09.583722 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1664, seq 0, length 64
    16:05:09.583912 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1664, seq 0, length 64

Pero poco despues de que el router `suplantador` le envia paquetes ICMP a `80.0.2.1` (el enganyado), el router `suplantado` deja de recibir los paquetes ICMP y *tcpdump* no muestra nada mas.

### router suplantador ###

En principio no recibe paquetes ICMP del `enganyado`, pero poco despues de enviarle paquetes ICMP al `enganyado`, *tcpdump* caputra:

    15:13:08.014349 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1668, seq 7, length 64
    15:13:08.014586 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1668, seq 7, length 64
    15:13:09.029454 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 0, length 64
    15:13:09.029742 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 0, length 64
    15:13:10.029825 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 1, length 64
    15:13:10.030062 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 1, length 64
    15:13:11.030204 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 2, length 64
    15:13:11.030450 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 2, length 64
    15:13:12.030577 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 3, length 64
    15:13:12.030819 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 3, length 64
    15:13:13.030977 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 4, length 64
    15:13:13.031203 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 4, length 64
    15:13:14.033006 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 5, length 64
    15:13:14.033242 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 5, length 64
    15:13:15.031744 IP 80.0.2.1 > 80.0.3.1: ICMP echo request, id 1670, seq 6, length 64
    15:13:15.031987 IP 80.0.3.1 > 80.0.2.1: ICMP echo reply, id 1670, seq 6, length 64

### Conclusiones ###

Esta prueba comprueba que el router `enganyado` ha dejado de enviar paquetes al router legitimo que es el `suplantado` y comienza a enviarlos al `suplantador`. Esto por que babeld ha cambiado la ruta hacia la IP `80.0.3.1` en el router enganyado por el anuncio del `suplantador` que se encuentra fisicamente mas cerca.

## babeld debug ##

Siguiendo el mismo escenario que la **prueba de envio de paquetes a destinos incorrectos**, pero ahora se hace un seguimiento a los mensajes del protocolo babel.

A continuacion los resultados del protocolo babel en cada router, se obtienen mediante:

    babeld -c /var/etc/babeld.conf -d 2

### router 80.0.3.1/24 (suplantado) ###

    My id 80:93:b4:ba:5a:b5:96:35 seqno 25568
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3800 rxcost 585 txcost 511 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach f800 rxcost 273 txcost 260 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 277 (4235) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 age 2 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 1424 (4747) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 chan (255) age 14 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1168 (4224) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 14 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 618 (4963) refmetric 341 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 chan (255) age 2 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Received nh 80.0.2.1 (1) from fe80::ea94:f6ff:fe6b:80fa on wlan0.
    Received router-id ea:94:f6:ff:fe:d9:fc:10 from fe80::ea94:f6ff:fe6b:80fa on wlan0.
    Received update for 80.0.3.0/24 from fe80::ea94:f6ff:fe6b:80fa on wlan0.
    
    My id 80:93:b4:ba:5a:b5:96:35 seqno 25568
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3800 rxcost 585 txcost 511 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach f800 rxcost 273 txcost 260 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 277 (4235) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 age 2 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 1424 (4747) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 chan (255) age 14 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1168 (4224) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 14 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1301 (4963) refmetric 1024 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 chan (255) age 0 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
      (flushing 1 buffered updates on wlan0 (35))
      (flushing 59 buffered bytes on wlan0)
    
    My id 80:93:b4:ba:5a:b5:96:35 seqno 25568
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3800 rxcost 585 txcost 511 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach f800 rxcost 273 txcost 260 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 277 (3589) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 age 3 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 1424 (3967) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 chan (255) age 15 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1168 (3614) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 15 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1301 (4250) refmetric 1024 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 chan (255) age 1 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Received hello 19473 (400) from fe80::ea94:f6ff:fe6b:80fa on wlan0.
    Received ihu 257 (1200) from fe80::ea94:f6ff:fe6b:80fa on wlan0 for fe80::62e3:27ff:fe4a:8de8.
    Received ihu 1024 (1200) from fe80::ea94:f6ff:fe6b:80fa on wlan0 for fe80::ea94:f6ff:fed9:fc10.
    
    My id 80:93:b4:ba:5a:b5:96:35 seqno 25568
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3800 rxcost 585 txcost 511 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach fc00 rxcost 264 txcost 257 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 265 (3589) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 age 3 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 1424 (3967) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50937 chan (255) age 15 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1168 (3614) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 15 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 1289 (4250) refmetric 1024 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 chan (255) age 1 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)

### router 80.0.2.1 (enganyado) ###

    My id 70:32:2b:59:e1:e4:dd:ed seqno 50936
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (256) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 3 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (installed)
      (flushing 8 buffered bytes on wlan0)
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50936
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (256) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 4 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (installed)
    Checking neighbours.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50936
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (256) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 6 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (installed)
    Sending hello 19421 (400) to wlan0.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50936
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (256) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 7 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (installed)
    Received hello 61261 (400) from fe80::62e3:27ff:fe4a:8de8 on wlan0.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50936
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (256) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 7 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (installed)
    
    .................................
    ... Despues de como 2 minutos ...
    
    Received hello 7664 (400) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 4 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    Sending hello 19525 (400) to wlan0.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 4 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    Expiring old routes.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 5 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
      (flushing 3 buffered updates on wlan0 (5))
      (flushing 70 buffered bytes on wlan0)
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 5 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    Received hello 7665 (400) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
    Received ihu 256 (1200) from fe80::ea94:f6ff:fed9:fc10 on wlan0 for fe80::ea94:f6ff:fe6b:80fa.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    Checking neighbours.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
    Sending hello 19526 (400) to wlan0.
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 9 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
      (flushing 8 buffered bytes on wlan0)
    
    My id 70:32:2b:59:e1:e4:dd:ed seqno 50937
    Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    80.0.2.0/24 metric 0 (exported)
    80.0.3.0/24 metric 256 (261) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1877 age 9 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)

## router 80.0.3.1/24 (suplantador) ##

    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach fff8 rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 001c rxcost 65535 txcost 1023 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 256 (262) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 12 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 52 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 52 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2304 (2299) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 28 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Checking neighbours.
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 7ffc rxcost 341 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 000e rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 341 (262) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 12 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 52 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 52 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2389 (2299) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 28 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    
    
    
    Checking kernel routes.
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 7ffc rxcost 341 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 000e rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 341 (279) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 13 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 53 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 53 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2389 (2314) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 29 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Expiring old routes.
    Sending hello 7590 (400) to wlan0.
    Sending ihu 341 on wlan0 to fe80::ea94:f6ff:fe6b:80fa.
    Sending ihu 65535 on wlan0 to fe80::62e3:27ff:fe4a:8de8.
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 7ffc rxcost 341 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 000e rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 341 (306) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 16 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 56 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 56 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2389 (2348) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 32 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
      (flushing 40 buffered bytes on wlan0)
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 7ffc rxcost 341 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 000e rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 341 (306) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 16 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 56 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 56 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2389 (2348) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 32 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Checking neighbours.
    Sending self update to wlan0.
    Sending update to wlan0 for 80.0.3.0/24 from ::/0.
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3ffe rxcost 512 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0007 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 512 (317) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 18 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 58 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 58 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2560 (2360) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 34 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3ffe rxcost 512 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0007 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 512 (317) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 18 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 58 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 58 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2560 (2360) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 34 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Sending hello 7591 (400) to wlan0.
    Sending ihu 512 on wlan0 to fe80::ea94:f6ff:fe6b:80fa.
    Sending ihu 65535 on wlan0 to fe80::62e3:27ff:fe4a:8de8.
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3ffe rxcost 512 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0007 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 512 (362) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 19 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 59 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 59 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2560 (2395) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 35 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
    Sending self update to wlan0.
    Sending update to wlan0 for 80.0.3.0/24 from ::/0.
    Sending update to wlan0 for any.
    Sending self update to wlan0.
    Sending update to wlan0 for 80.0.3.0/24 from ::/0.
    Sending update to wlan0 for any.
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3ffe rxcost 512 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0007 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 512 (385) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 20 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 60 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 60 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2560 (2431) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 36 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
      (flushing 4 buffered updates on wlan0 (11))
      (flushing 102 buffered bytes on wlan0)
    
    My id ea:94:f6:ff:fe:d9:fc:10 seqno 1876
    Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3ffe rxcost 512 txcost 256 rtt 0.000 rttcost 0 chan 7.
    Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0007 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
    80.0.3.0/24 metric 0 (exported)
    80.0.2.0/24 metric 512 (385) refmetric 0 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 age 20 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
    80.0.2.0/24 metric 65535 (65535) refmetric 256 id 70:32:2b:59:e1:e4:dd:ed seqno 50936 chan (255) age 60 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1
    80.0.3.0/24 metric 65535 (65535) refmetric 0 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 age 60 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
    80.0.3.0/24 metric 2560 (2431) refmetric 2048 id 80:93:b4:ba:5a:b5:96:35 seqno 25568 chan (255) age 36 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)

## Observaciones preliminares ##

Se puede ver que el router `suplantado` y `suplantador` ambos se asocian con el `enganyado` y se descubren entre si marcandose como alcanzables hacia el prefijo 80.0.3.0/24 (ambos tienen el mismo)

EL router `enganyado` al parecer prefiere usar al primer router con el que ha hecho enlace (el `suplantado`), pero en los reportes de arriba se ve que despues de cierto tiempo ha escogido al router `suplantador` como ruta hacia 80.0.3.0/24  que ha sido tomada ilegitimante del router `suplantado` por el `suplantador`.

Las pruebas mediante el envio de paquetes ICMP comprueban que el `enganyado` envia los paquetes al `suplantador`, lo que comprueba que el protocolo babel (en particular la implementacion *babeld-1.8.0*) **es vulnerable** a ataques de suplantacion de identidad.
