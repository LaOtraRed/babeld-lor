## Some notes on babel and babeld ##

### Babel TLVs ###

    |-------+-----------------+-----------------------------------------------------------------------|
    | Value | Name            | Description                                                           |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     0 | Pad1            |                                                                       |
    |     1 | PadN            |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     2 | Acknowledgement | This TLV requests that the receiver send an Acknowledgement TLV       |
    |       | Request         | within the number of centiseconds specified by the Interval field.    |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     3 | Acknowledgement | This TLV is sent by a node upon receiving an Acknowledgement Request. |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     4 | Hello           | This TLV is used for neighbour discovery and for determining a link's |
    |       |                 | reception cost.                                                       |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     5 | IHU             | An IHU ("I Heard You") TLV is used for confirming bidirectional       |
    |       |                 | reachability and carrying a link's transmission cost.                 |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     6 | Router-Id       | A Router-Id TLV establishes a router-id that is implied by subsequent |
    |       |                 | Update TLVs.                                                          |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     7 | Next Hop        | A Next Hop TLV establishes a next-hop address for a given address     |
    |       |                 | family (IPv4 or IPv6) that is implied by subsequent Update TLVs.      |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     8 | Update          | An Update TLV advertises or retracts a route.  As an optimisation,    |
    |       |                 | this can also have the side effect of establishing a new implied      |
    |       |                 | router-id and a new default prefix.                                   |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |     9 | Route Request   | A Route Request TLV prompts the receiver to send an update for a      |
    |       |                 | given prefix, or a full routing table dump.                           |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|
    |    10 | Seqno Request   | A Seqno Request TLV prompts the receiver to send an Update for a      |
    |       |                 | given prefix with a given sequence number, or to forward the request  |
    |       |                 | further if it cannot be satisfied locally.                            |
    |       |                 |                                                                       |
    |-------+-----------------+-----------------------------------------------------------------------|

#### Notes about TLVs ####

It is most likely that there will be defined a new TLV to the authenticacion method: `lor-auth` TLV.

See [babel-integration.es.md](babe-integration.es.md)

### babel update ###

[From babel RFC - section 3.5]
> Conceptually, a Babel update is a quintuple (prefix, plen, router-id,
> seqno, metric), where (prefix, plen) is the prefix for which a route
> is being advertised, router-id is the router-id of the router
> originating this update, seqno is a nondecreasing (modulo 2^16)
> integer that carries the originating router seqno, and metric is the
> announced metric.

#### False routes ####

##### Considering feasibility condition #####

If an entry (prefix, plen, router-id, seqno, metric) exists in the `source table`:

 * A node anounces an entry (prefix, plen, router-id, seqno', metric') where:
 
   * seqno' < seqno
         OR
   * seqno = seqno' and metric < metric'

**Then for a distance to become feasible it will only take to announce seqno' > seqno**

##### Considering Route acquisition #####

> When a Babel node receives an update (id, prefix, seqno, metric) from
> a neighbour neigh with a link cost value equal to cost, it checks
> whether it already has a routing table entry indexed by (neigh, id,
> prefix).
> If no such entry exists:
>
> o  if the update is unfeasible, it is ignored;
> 
> o  if the metric is infinite (the update is a retraction), the update
>    is ignored;
> 
> o  otherwise, a new route table entry is created, indexed by (neigh,
>    id, prefix), with seqno equal to seqno and an advertised metric
>    equal to the metric carried by the update.
> 
> If such an entry exists:
> 
> o  if the entry is currently installed and the update is unfeasible,
>    then the behaviour depends on whether the router-ids of the two
>    entries match.  If the router-ids are different, the update is
>    treated as though it were a retraction (i.e., as though the metric
>    were FFFF hexadecimal).  If the router-ids are equal, the update
>    is ignored;
> 
> o  otherwise (i.e., if either the update is feasible or the entry is
>    not currently installed), then the entry's sequence number,
>    advertised metric, metric, and router-id are updated and, unless
>    the advertised metric is infinite, the route's expiry timer is
>    reset to a small multiple of the Interval value included in the
>    update.
> 
> When a route's expiry timer triggers, the behaviour depends on
> whether the route's metric is finite.  If the metric is finite, it is
> set to infinity and the expiry timer is reset.  If the metric is
> already infinite, the route is flushed from the route table.
> 
>    After the routing table is updated, the route selection procedure
>    (Section 3.6) is run.


