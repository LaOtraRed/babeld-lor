# Bifurcación del método de autenticación con el protocolo babel #

Estado: *Experimental*

* No se sigue el Mecanismo de extensión del protocolo babel (RFC 7557),
* Se define un nuevo TLV denominado **lorauth**.
* Se modifica el TLV Update, para lidiar con problemas de IP spoofing
en ARP en redes ad-hoc por ejemplo, esto se describe mas adelante.

## Estructura del TLV lorauth ##

     0                   1 
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |     Type=29   |   Length      |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 |        Np     |  Clen         |
	 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	 |            Cipher...
	 +-+-+-+-+-+-+-+-+-+-+-+-+-

Donde:

* **Type**: Es siempre 29.
* **Length**: Es el número de bytes del TLV.
* **Np**: Es el número de paquete del mensaje cifrado siendo enviado.
* **Clen**: El número de bytes de la parte del mensaje cifrado `Cipher`.
* **Cipher**: La parte del mensaje cifrada con la llave RSA privada `PK` de
  la entidad `C`. Esta cadena esta en función del `router-id` y `seqno`
  extraídos del `TLV Router-id` y el `TLV Update`.

Como el mensaje cifrado completo puede tener un tamaño mayor 255 bytes, se
envía en mensaje cifrado en paquetes de tamaño máximo de hasta 254 bytes, y
Np especifica el número de paquete o número de parte del mensaje cifrado
siendo enviado.

Por ejemplo si el mensaje cifrado tiene un total de 512 bytes, se enviará un
total de 3 paquetes:

* 1er paquete: `(Type=29, Length=255, Np=1, Clen=253, Cipher-parte1)`
* 2do paquete: `(Type=29, Length=255, Np=2, Clen=253, Cipher-parte2)`
* 3er paquete: `(Type=29, Length=8, Np=3, Clen=6, Cipher-parte3)`

Se divide en paquetes para matener el campo `Length` a 1 octeto, y seguir
el estandar del [RFC - 6126, sección 4.3](https://tools.ietf.org/html/rfc6126#section-4.3)

## ¿Cuando se utiliza el TLV lorauth? ##

Se utiliza antes de cada `TLV Update` necesariamente (el TLV Update esta
descrito en la
[sección 4.4.9, RFC 6126](https://tools.ietf.org/html/rfc6126#section-4.4.9)
, la especificación del protocolo original), si se recibe
un `TLV Update` al cual no le precede inmediatamente antes el
`TLV lorauth` se ignora el Update.

Todos los `TLV Update` requieren que se incluya el `TLV lorauth`.

Se requieren verificar el `router-id`, `seqno` y prefijo que el nodo anuncia.

* El `router-id` se obtiene del `TLV Router-id` que precede al Update.
* El `seqno` y prefijo (`prefix`) se obtienen del `TLV Update`.

En cada `TLV Update` se seguirá el
[procedimiento de autenticación](README.es.md#Procedimiento_de_autenticación_entre_nodos)

## Modificaciones al protocolo babel original ##

Cuando se está usando la autenticación, se requieren hacer ciertas
modificaciones al protocolo original.

### La Tabla de fuentes (The Source Table) ###

Se modifica la tabla de fuentes (*source table* originalmente
descrita en la sección 3.2.4 del RFC 6126) para almacenar datos
relacionados a la autenticación.

Se agregan dos elementos a cada registro en la tabla de fuentes de
babel, estos dos elementos son `clen` y `cipher`, quedando cada
registro de la siguiente forma:

    (prefix, plen, router-id, seqno, metric, clen, cipher)

* `clen`: Es el número de bytes que ocupa `cipher`.
* `cipher`: Es la última cadena de cifrado que se ha autenticado y que
  satisface `prefix` y el `seqno` del registro correspondiente.

Así cuando se hace un Update se agrega el `TLV lorauth` usando los
elementos `clen` y `cipher` guardados en esta tabla.

Los demás elementos del registro se manejan como en el protocolo
original.

### Selección cadena de cifrado ###

Cada nodo ha recibido de la entidad `C` un conjunto de cadenas cifradas
con `k` elementos.

El problema es que si un nodo captura todos los `TLV lorauth` de un
nodo específico, estará en condiciones de suplantar su identidad. Esto
por que si posee las cadenas de autenticación de un nodo `A`, podrá
anunciar el prefijo del nodo `A` y usar las cadenas de autenticación de
`A` en los `TLV lorauth`. En este caso los nodos que reciban los
*Update* del suplantador comprobarán que `Cipher` en los `TLV lorauth`
son válidos y los aceptarán.

Dado que capturar paquetes en redes inalámbricas es sencillo, para
dificultar el trabajo de obtener todas las cadenas de cifrado de un
nodo A, se puede hacer que el conjunto de cadenas cifradas `c_A` para un
nodo A tenga un orden específico y se envíe de acuerdo al `seqno` y
`router-id` en el *Update* correspondiente.

#### Contenido de cada cadena cifrada ####

Cada cadena cifrada usada por un nodo tiene la forma:

    prefix + "_" + rand6d() + fd(fs(router_id, seqno), nd)

* **prefix**: Es el prefijo o `prefix` asignado al nodo.
* **rand6d()**: Una función que retorna 6 dígitos al azar completando
  ceros, por ejemplo; 041882, 118525, 000095
* **fd(n, nd)**: Una función que aplica: `n % (10^nd)` y lo expresa en
  un número de dígitos igual a `nd`, por ejemplo:
  fd(2012, 2) = 12 , fd(995142, 3) = 142, fd(9101, 2) = 01
* **fs(router_id, seqno)**: Una función que se define como:

  `((seqno*7)+1) xor (((router_id>>32)>>3) & (router_id&0xFFFFFFFF))`
  
  `xor` es la operacion lógica or exclusivo, `>>` es el desplazamiento hacia
  la derecha

La entidad `C` crea un conjunto de cadenas cifradas para un nodo A
de tal forma que se tengan:

    c_A = { prefix_A + "_" + rand6d() + "00",
	        prefix_A + "_" + rand6d() + "01",
			.
			.
			.
			prefix_A + "_" + rand6d() + "99" }

Este caso es cuando el número de elementos `c_A` es `100`.

#### Cadena de cifrado enviada en cada TLV lorauth ####

Del resultado de `fd(fs(router_id, seqno), nd)` se selecciona el
número de elemento a enviar del conjunto de cadenas cifradas.

Por ejemplo si: router_id=0x1122334455667788, seqno=0x1955
resulta en el numero de elemento o indice 20. Entonces se usará la
cadena `c_A[20]` para el `TLV lorauth`. 

Si: router_id=0x1122334455667788, seqno=0x1956 resulta en el
índice 11, se usará `c_A[11]`.

Para el caso de que el número de elementos es 100, si un nodo B recibe
un `TLV lorauth` de un nodo A seguirá los siguientes pasos:

1. Se trata de descifrar `Cipher` usando `CP` (clave pública de `C`).
2. Comprobar que en el mensaje descifrado esté incluído `prefix_A`
   que se anuncia en el *Update*.
3. Comprobrar que los dos útimos dígitos del mensaje descifrado
   sean `fd(fs(router_id, seqno), 2)`, donde `router_id` y `seqno`
   son los que se anuncian en el *Update*.

Si se cumplen estas tres condiciones la autenticación del *Update*
dado está completada, si no, se ignorará el *Update* correspondiente.

Con esta medida se dificulta el trabajo de obtener el orden correcto de
las cadenas cifradas `c_A` para un nodo A. Y dado que el método de
autenticación también comprueba el orden de la cadena descifrada se
hace más difícil el trabajo de suplantar completamente a un nodo A con
sólo capturar y reenviar los paquetes de autenticación.

## Modificación al TLV Update ###

La implementación original del protocolo Babel (sección 4.4.9 RFC - 6126)
lo define:

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |    Type = 8   |    Length     |       AE      |    Flags      |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |     Plen      |    Omitted    |            Interval           |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |             Seqno             |            Metric             |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |      Prefix...
    +-+-+-+-+-+-+-+-+-+-+-+-

En esta modificación se define así:

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |    Type = 8   |    Length     |       AE      |    Flags      |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |     Plen      |    Omitted    |            Interval           |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |             Seqno             |            Metric             |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	|                        Cipher_resume                          |
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |      Prefix...
    +-+-+-+-+-+-+-+-+-+-+-+-

Donde todos los campos salvo **Cipher_resume** se entienden de forma
similar al TLV Update original.

* **Cipher_resumen**: Son cuatro octetos que contiene los dos primeros y
dos últimos bytes del campo **Cipher** que han sido enviados en un TLV lorauth
previo, por ejemplo.

Si Cipher = `4A2DF11...89` (512 bytes), Cipher_resumen = `4A89`.

Con este nuevo campo se espera conseguir hacer que un nodo con el protocolo
Babel original, no entienda un TLV Update enviado.

## Sobre  ARP IP Spoofing ##

Se define **babeld-lor** como la versión modificada del protocolo en este
trabajo y se define **babeld** como la implementación original del protocolo
Babel.

Cuando al mantener la compatibilidad con el protocolo original, cuando un
nodo con babeld-lor envía updates que un nodo con babeld puede entender,
este instalará rutas hacia los nodos babeld-lor ignorando los TLV lorauth.
Los nodos babeld-lor no instalarán rutas hacia los nodos babeld por que estos
no usan el TLV-lorauth.

Sin embargo el problema se da al nivel de capa 2 del modelo TCP/IP. Cuando un
nodo con babeld tiene una ruta hacia un nodo babeld-lor y estos tengan conexión
por ejemplo en una red ad-hoc, solo basta que un nodo suplantador (un nodo que
utiliza la misma dirección IP que un nodo babeld-lor) haga una petición ARP
hacia un nodo con babeld-lor. Al estar en la misma red y ser visibles entre si
el nodo con babeld-lor responderá la petición ARP y agregará a su tabla ARP la
dirección MAC del nodo babeld-lor estableciendo conexión.

Se da el mismo caso cuando un nodo babeld-lor con prefijo 80.0.2.1 hace una
petición ARP hacia por ejemplo a un prefijo 80.0.1.1 para hacer comunicación a
una ruta instalada de otro nodo babeld-lor con quién se ha autenticado a nivel
de protocolo Babel.

Como hay otro nodo suplantador que usa babeld, con el prefijo 80.0.1.1, está
en la misma red y tiene una ruta instalada hacia 80.0.2.1. Es probable que
el nodo babeld sea el primero en responder la petición ARP y el nodo con
babeld-lor agregará en su tabla ARP la MAC del suplantador creyendo que este
es el nodo babeld-lor con quién se ha autenticado antes.

Este problema esta fuera del alcance del protocolo Babel ya que este trabaja
en la capa 3.

Para mitigar este problema al hacer esta bifurcación y modificación al TLV
Update del protocolo Babel original, un nodo babeld no debería poder establecer
rutas automáticamente hacia nodos babeld-lor por no entender estas
actualiaciones de rutas (TLV Update) y así no debería responder a peticiones
ARP por no tener rutas instaladas hacia nodos babeld-lor.

#### Observaciones ####

* Cada nodo que participa en la red esta obligado a autenticarse ya que
  para que otros nodos tomen en cuenta el Update (actualizaciones de
  rutas) debe emitir cadenas `Cipher` válidas y los nodos que anuncian
  rutas de terceros **necesitan** reenviar las cadenas `Cipher` que ha
  confirmado como válidas.
* Cada nodo debería guardar la última cadena `Cipher` de cada nodo que
  autentica.
* Se requiere procesamiento extra para descifrar las cadenas `Cipher`
  con el algoritmo RSA.