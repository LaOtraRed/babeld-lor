## IP spoofing scenario number 1 ##

The first IP spoofing scenario is like the following:

        .---.  TLV + prefix_A + c_A[i]     .---.            .---.
        | A |----------------------------> | B |<-----------| K |
        '---'                              '---'            '---'
      prefix: 80.0.3.1/24           prefix: 80.0.2.1/24     prefix: 80.0.3.1/24
	  router-id: 20                 router-id: 30           router-id: 20


`A`, `B` are legitimate routers anouncing their prefixes: 80.0.2.1/24 respectively.
Then an attacker `K` copies `A` prefix: `80.0.2.1/24` and router-id: `20` and announces it in the hope that after `B` receives these announces it will send packages to `K` instead to `A`.


## resultados parciales ##

### en router 80.0.3.1/24 (victima) ###

My id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 03ff rxcost 8192 txcost 16448 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 4c:77:ce:8c:5a:23:36:84 seqno 50931 age 32 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 256 (381) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 1091 id ea:94:f6:ff:fe:d9:fc:10 seqno 1870 age 31 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 256 (341) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 1091 id ea:94:f6:ff:fe:d9:fc:10 seqno 1870 chan (255) age 31 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa

My id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 03ff rxcost 8192 txcost 16448 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 4c:77:ce:8c:5a:23:36:84 seqno 50931 age 32 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 256 (381) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 1091 id ea:94:f6:ff:fe:d9:fc:10 seqno 1870 age 31 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 256 (341) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 7 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 1091 id ea:94:f6:ff:fe:d9:fc:10 seqno 1870 chan (255) age 31 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa

My id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 03ff rxcost 8192 txcost 16448 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 4c:77:ce:8c:5a:23:36:84 seqno 50931 age 36 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 256 (305) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 11 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 1091 id ea:94:f6:ff:fe:d9:fc:10 seqno 1870 age 35 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 256 (294) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 11 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 1091 id ea:94:f6:ff:fe:d9:fc:10 seqno 1870 chan (255) age 35 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa

### router 80.0.2.1/24 (enganyado) ###

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach f800 rxcost 273 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3fff rxcost 512 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 682 (3030) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (6327) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 3 via wlan0 neigh fe80::2e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 682 (2578) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (5751) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 12 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 938 (3376) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (5242) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 3 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach f800 rxcost 273 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3fff rxcost 512 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 682 (2689) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (5302) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 4 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 682 (2134) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (4592) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 13 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 938 (2882) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (4497) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 4 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fc00 rxcost 264 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3fff rxcost 512 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 682 (2689) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (5302) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 4 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 682 (2134) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (4592) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 13 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 938 (2882) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 13 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (4497) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 4 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fc00 rxcost 264 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3fff rxcost 512 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 682 (1707) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (3118) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 7 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 682 (1437) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (2652) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 16 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 938 (1997) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (2282) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 7 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fc00 rxcost 264 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 3fff rxcost 512 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 682 (1707) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (3118) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 7 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 682 (1437) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (2652) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 16 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 938 (1997) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (2282) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 7 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fc00 rxcost 264 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 1fff rxcost 1024 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 1364 (1540) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (2678) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 1364 (1263) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (2305) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 17 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 1620 (1797) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (1900) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fc00 rxcost 264 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 1fff rxcost 1024 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 1364 (1540) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (2678) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 1364 (1263) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (2305) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 17 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 1620 (1797) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (1900) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fc00 rxcost 264 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 1fff rxcost 1024 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 1364 (1540) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (2678) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 1364 (1263) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (2305) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 17 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 1620 (1797) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (1900) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fe00 rxcost 260 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 1fff rxcost 1024 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 1364 (1540) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (2678) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 1364 (1263) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (2305) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 17 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 1620 (1797) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 17 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (1900) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 8 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fe00 rxcost 260 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 1fff rxcost 1024 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 1364 (1500) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 18 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (2328) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 9 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 1364 (1279) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 18 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (1923) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 18 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 1620 (1763) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 18 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (1638) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 9 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

My id 4c:77:ce:8c:5a:23:36:84 seqno 50931
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach fe00 rxcost 260 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 1fff rxcost 1024 txcost 341 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 1364 (1500) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 18 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
80.0.3.0/24 metric 273 (2328) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 9 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 nexthop 80.0.3.1 (feasible)
fc01:1934:fffe::101/128 metric 1364 (1279) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 age 18 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 529 (1923) refmetric 256 id ea:94:f6:ff:fe:d9:fc:10 seqno 1871 chan (255) age 18 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)
fc01:1934:fffe:9497::8001/128 metric 1620 (1763) refmetric 256 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 chan (255) age 18 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe:9497::8001/128 metric 273 (1638) refmetric 0 id f8:b0:d6:c8:cd:0e:a3:05 seqno 25564 age 9 via wlan0 neigh fe80::62e3:27ff:fe4a:8de8 (feasible)

### router 80.3.0.1/24 (atacante) ###

(no capture los datos)

## nueva prueba ##

### router 80.3.0.1/24 (suplantado) ###

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3e07 rxcost 528 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 0ffb rxcost 2050 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 528 (862) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 24 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 25 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 25 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 25 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 25 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 25 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 3e07 rxcost 528 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 0ffb rxcost 2050 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 528 (804) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 25 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)
Checking neighbours.
Sending update to wlan0 for 80.0.2.0/24 from ::/0.

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 1f03 rxcost 1056 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 07fd rxcost 4100 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 1056 (804) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 25 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)
  (flushing 1 buffered updates on wlan0 (35))

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 1f03 rxcost 1056 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 07fd rxcost 4100 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 1056 (804) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 25 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)
  (flushing 75 buffered bytes on wlan0)

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 1f03 rxcost 1056 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 07fd rxcost 4100 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 1056 (804) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 25 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 26 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 26 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)
Sending hello 8101 (400) to wlan0.
Sending ihu 1056 on wlan0 to fe80::ea94:f6ff:fe6b:80fa.
Sending ihu 4100 on wlan0 to fe80::ea94:f6ff:fed9:fc10.

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 1f03 rxcost 1056 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 07fd rxcost 4100 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 1056 (918) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 28 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 29 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 29 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 29 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 29 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 29 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)
  (flushing 40 buffered bytes on wlan0)

My id f0:70:af:33:fa:04:a2:09 seqno 25565
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 1f03 rxcost 1056 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach 07fd rxcost 4100 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe:9497::8001/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 1056 (918) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 28 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.2.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 29 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 29 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (feasible)
80.0.3.0/24 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 29 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 age 29 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
fc01:1934:fffe::101/128 metric 65535 (65535) refmetric 65535 id ea:94:f6:ff:fe:d9:fc:10 seqno 1872 chan (255) age 29 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (feasible)


### router 80.3.0.1/24 (suplantador) ###

My id ea:94:f6:ff:fe:d9:fc:10 seqno 1873
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 000f rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe::101/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 59 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 59 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe:9497::8001/128 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 75 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (installed)
  (flushing 24 buffered bytes on wlan0)

My id ea:94:f6:ff:fe:d9:fc:10 seqno 1873
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 000f rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe::101/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 60 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 60 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe:9497::8001/128 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 76 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (installed)
Sending hello 32375 (400) to wlan0.
Sending ihu 65535 on wlan0 to fe80::ea94:f6ff:fe6b:80fa.

My id ea:94:f6:ff:fe:d9:fc:10 seqno 1873
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 000f rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe::101/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 64 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 64 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe:9497::8001/128 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 80 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (installed)
Sending self update to wlan0.
Sending update to wlan0 for fc01:1934:fffe::101/128 from ::/0.
Sending update to wlan0 for 80.0.3.0/24 from ::/0.
Sending update to wlan0 for any.
Sending self update to wlan0.
Sending update to wlan0 for fc01:1934:fffe::101/128 from ::/0.
Sending update to wlan0 for 80.0.3.0/24 from ::/0.
Sending update to wlan0 for any.

My id ea:94:f6:ff:fe:d9:fc:10 seqno 1873
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 000f rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe::101/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 64 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 64 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe:9497::8001/128 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 80 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (installed)
  (flushing 6 buffered updates on wlan0 (8))
  (flushing 148 buffered bytes on wlan0)

My id ea:94:f6:ff:fe:d9:fc:10 seqno 1873
Neighbour fe80::ea94:f6ff:fe6b:80fa dev wlan0 reach 000f rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
fc01:1934:fffe::101/128 metric 0 (exported)
80.0.3.0/24 metric 0 (exported)
80.0.2.0/24 metric 65535 (65535) refmetric 0 id 08:22:57:a2:72:ac:6c:ce seqno 50933 age 64 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (installed)
80.0.3.0/24 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 64 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa nexthop 80.0.2.1 (feasible)
fc01:1934:fffe:9497::8001/128 metric 65535 (65535) refmetric 1365 id f0:70:af:33:fa:04:a2:09 seqno 25566 chan (255) age 80 via wlan0 neigh fe80::ea94:f6ff:fe6b:80fa (installed)

### router 80.2.0.1/24 (enganyado) ###

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 000b rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (731) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 8 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (637) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 8 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
  (flushing 24 buffered bytes on wlan0)

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 000b rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (637) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 9 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (555) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 9 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Checking neighbours.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0005 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (554) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 10 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (488) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 10 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Received hello 32404 (400) from fe80::ea94:f6ff:fed9:fc10 on wlan0.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0005 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (470) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (412) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Sending hello 24693 (400) to wlan0.
Sending ihu 256 on wlan0 to fe80::ea94:f6ff:fed9:fc10.
Sending ihu 65535 on wlan0 to fe80::62e3:27ff:fe4a:8de8.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0005 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (470) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (412) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
  (flushing 40 buffered bytes on wlan0)

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0005 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (470) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (412) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 12 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)

Checking neighbours.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0002 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (374) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 15 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (337) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 15 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Expiring old routes.
Sending self update to wlan0.
Sending update to wlan0 for f01:1934:fffe::201/128 from ::/0.
Sending update to wlan0 for 80.0.2.0/24 from ::/0.
Sending update to wlan0 for any.
Sending self update to wlan0.
Sending update to wlan0 for f01:1934:fffe::201/128 from ::/0.
Sending update to wlan0 for 80.0.2.0/24 from ::/0.
Sending update to wlan0 for any.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0002 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (358) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (320) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 16 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Received hello 32405 (400) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received nh 80.0.3.1 (1) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received router-id 08:22:57:a2:72:ac:6c:ce from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received update for 80.0.2.0/24 from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received router-id ea:94:f6:ff:fe:d9:fc:10 from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received update/prefix for fc01:1934:fffe::101/128 from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received update for 80.0.3.0/24 from fe80::ea94:f6ff:fed9:fc10 on wlan0.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0002 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (358) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 0 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (320) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 0 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Sending hello 24694 (400) to wlan0.
Sending ihu 65535 on wlan0 to fe80::62e3:27ff:fe4a:8de8.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0002 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (358) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 0 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (320) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 0 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
  (flushing 6 buffered updates on wlan0 (5))
  (flushing 142 buffered bytes on wlan0)

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 258 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0002 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 258 (344) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 1 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 258 (308) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 1 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Received hello 32406 (400) from fe80::ea94:f6ff:fed9:fc10 on wlan0.
Received ihu 256 (1200) from fe80::ea94:f6ff:fed9:fc10 on wlan0 for fe80::ea94:f6ff:fe6b:80fa.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
Neighbour fe80::62e3:27ff:fe4a:8de8 dev wlan0 reach 0002 rxcost 65535 txcost 65535 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 256 (315) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 3 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 256 (289) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 3 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Checking neighbours.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 256 (302) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 4 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 256 (284) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 4 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
Sending hello 24695 (400) to wlan0.

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 256 (295) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 5 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 256 (278) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 5 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)
  (flushing 8 buffered bytes on wlan0)

My id 08:22:57:a2:72:ac:6c:ce seqno 50933
Neighbour fe80::ea94:f6ff:fed9:fc10 dev wlan0 reach ffff rxcost 256 txcost 256 rtt 0.000 rttcost 0 chan 7.
f01:1934:fffe::201/128 metric 0 (exported)
80.0.2.0/24 metric 0 (exported)
80.0.3.0/24 metric 256 (295) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 5 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 nexthop 80.0.3.1 (installed)
fc01:1934:fffe::101/128 metric 256 (278) refmetric 0 id ea:94:f6:ff:fe:d9:fc:10 seqno 1873 age 5 via wlan0 neigh fe80::ea94:f6ff:fed9:fc10 (installed)

 
