# Método de autenticación del protocolo babel para LaOtraRed #

Como en el proyecto LaOtraRed se utilizan direcciones IPv4 estáticas en cada
nodo y a cada nodo se le asigna un bloque de direcciones fija
(prefijo público), la finalidad de este método de autenticación es reducir el
éxito de ataques de suplantación de identidad (IP spoofing) en cada nodo de
la red.

## ¿Cómo funciona? ##

Se tiene el siguiente modelo:

                                   (ooOOooo)
    +---+                         (         )
    | C |----------------------->(  Nodos en )
    +---+                       (  LaOtraRed  )
    PK,CP                        (           )
                                  (ooOOoOOoo)

Existe una entidad **C**, que posee un par de claves criptográficas , una clave
privada y una pública. La clave privada se denomina **PK** y la clave pública
**CP**.

**C** tiene la funcion de crear cadenas cifradas usando una llave privada
**PK**, **C** genera un conjunto de cadenas cifradas único por cada nuevo nodo
en la red.

Cada nodo en la red recibe de **C** sólo su conjunto de cadenas
cifradas correspondientes, junto con la llave pública **CP**.

Cada nodo de la red usa el conjunto de cadenas asignadas por **C** para
demostrar que se la ha asignado el bloque de direcciones IP que anuncia y los
demás nodos usan la clave pública **CP** para comprobar la autenticidad de
estos mensajes.

### Agregación de un nodo a la red ###

El proceso de agregación de un nuevo nodo miembro requiere que **C** primero
asigne un bloque de direcciones IP único para este nuevo nodo, más un
CIDR que puede ser /24 /25 /26 /27 ó /28 (IP /CIDR se entiende como prefijo)

A partir del prefijo asignado al nuevo nodo, **C** genera un conjunto de
cadenas cifradas para el nuevo nodo, luego mediante un canal de comunicacíon
privado **C** le proporciona al nuevo nodo el conjunto de cadenas recién
generado más la clave pública **CP**.

El conjunto de cadenas cifradas generadas para un nodo **A** se denomina
**c_A** y el prefijo asignado a **A** se denomina **prefix_A**.

    +---+     CP,c_A,prefix_A  .---.
    | C |--------------------> | A |
    +---+                      '---'
    (PK,CP)

El nuevo nodo **A** mantiene en secreto el conjunto **c_A** y lo utilizará
para autenticarse con los demás nodos de la red, tambíen conserva **CP** para
comprobar los mensajes de autenticación de otros nodos de la red.

Finalmente **A** usará **prefix_A** para anunciar su bloque de direcciones
IP a los otros nodos de la red.

### Procedimiento de autenticación entre nodos ###

Un nodo **A** se autentica con un nodo **B** cuando muestra que es propietario
legítimo del bloque de direcciones IP que anuncia (**prefix_A**).

El nodo **A** envía una cadena de autenticación cifrada a **B**, esta cadena de
autenticación está contenida en un TLV babel exclusivo para la autenticación. A
este TLV se lo denomina a partir de ahora como el TLV **lor-auth**.

* El TLV lor-auth se especifica en:
[babel-integration.es.md](babel-integration.es.md)

Se puede ilustrar el procedimiento de autenticación así:


        .---.  prefix_A + c_A[i]           .---.
        | A |----------------------------> | B |
        '---'                              '---'
      prefix_A                             prefix_B
      c_A                                  c_B
      CP                                   CP

 - `prefix_A` el bloque de direcciones IP que anuncia el nodo **A**
 - `c_A` conjunto cadenas de autenticación de **A**
 - `CP` la clave pública de **C**.

 - `prefix_B` el bloque de direcciones IP que anuncia el nodo **B**
 - `CP` la clave pública de **C**.
 - `c_B` conjunto cadenas de autenticación de **B**

 - `c_A[i]` es una cadena de autenticación con ínidce `i` del conjunto `c_A`

Al recibir **B** `prefix_A` y `c_A[i]` usa **CP** para descifrar `c_A[i]`, si
**B** consigue descifrar este mensaje significa que **A** ha recibido sus
cadenas de autenticación de **C**. El resultado de descifrar `c_A[i]` es
`d_A[i]`.

Como medida de seguridad adicional, **B** comprueba que `prefix_A` este dentro
de `d_A[i]`, si se cumple esta condición, el nodo **B** reconoce a **A** como
auténtico y poseedor legítimo de  `prefix_A`.

Si no se cumple esta condición o **B** no logra descifrar el mensaje de **A**,
ignorará este anuncio de **A** y la autenticación no se habrá completado.

## Bifurcación del método de autenticación en el protocolo babel ##

[forked-babel.es.md](forked-babel.es.md)

## Escenarios de suplantación ##

Se ha conseguido mostrar que babel es vulnerable a ataques de suplantación
de indentidad, en las pruebas descritas en:

[spoofing-tests1.md](spoofing-tests1.md)

Captura de paquetes de ataques de suplantación de identidad sobre el protocolo
*Babel original* (babeld)

[doc-lorauth/IP-spoofing-attacks/babeld](doc-lorauth/IP-spoofing-attacks/babeld)

Captura de paquetes de ataques de suplantación de identidad sobre el protocolo
*Babel modifcado* (babeld-lor)

[doc-lorauth/IP-spoofing-attacks/babeld-lor](doc-lorauth/IP-spoofing-attacks/babeld-lor)

## Notas sobre babel y babeld ##

[babel-notes.md](babel-notes.md)
