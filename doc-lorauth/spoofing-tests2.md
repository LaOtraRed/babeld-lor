# Pruebas de suplantacion 1 #

Se tiene tres routers:

        suplantado                       enganyado               suplantador
        .---.                              .---.                   .---.
        | A |----------------------------> | B |<------------------| K |
        '---'                              '---'                   '---'
      prefix: 80.0.3.1/24           prefix: 80.0.2.1/24          prefix: 80.0.3.1/24
	  router-id: d808da2263c271f8   router-id: 5043ebc68eaabc55  router-id: ea94f6fffed9fc10
	  MAC:62:e3:27:4a:8d:e8         MAC: ea:94:f6:6b:80:fa       MAC: e8:94:f6:d9:fc:10

* **suplantado** que es el legitimo acreedor del prefijo `80.0.3.1/24`
* **suplantador** que ha copiado el prefijo `80.0.3.1/24` del suplantado.
* **enganyado** que recibe anuncios de ambos routers que claman ser acreedores de `80.0.3.1/24` y este decide con quien comunicarse.

El unico cambio en la configuracion de babel ha sido, hacer que el router suplantador anuncie el mismo prefijo que el router suplantado en este caso `80.0.3.1/24`.

Se ha monitorizado el trafico entre estos 3 enrutadores con **wireshark**.

(TODO: pegar aqui los paquetes capturados en wireshark (ya estan marcados))

## Modo de prueba ##

Al principio de la prueba, sólo se tienen encendidos los routers `suplantado` y `enganyado`, después se enciende el router `suplantador` intencionalmente más cerca del `enganyado`.

Finalmente para comprobar que el router `enganyado` ha preferido usar la ruta hacia `80.0.3.1` a través del router `suplantador` en lugar del `suplantado`, se hace que el router enganyado envíe paquetes ICMP a la dirección IP 80.0.3.1.

Se monitoriza el destino y contenido de los paquetes babel e ICMP enviados entre estos tres enrutadores, mediante wireshark.

### Resultado de análisis de paquetes ###

(TODO: continuar)
