/* 
   babeld-lor
    Copyright (C) 2017  Rodrigo Garcia

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define LORAUTH_TOKENS_DIR "tokens"
#define LORAUTH_TOKENS_FILE "ciphered-tokens.ctxt"

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>
#define mbedtls_printf     printf
#define mbedtls_exit       exit
#define MBEDTLS_EXIT_SUCCESS EXIT_SUCCESS
#define MBEDTLS_EXIT_FAILURE EXIT_FAILURE
#endif

#if defined(MBEDTLS_BIGNUM_C) && defined(MBEDTLS_RSA_C) && \
    defined(MBEDTLS_FS_IO) && defined(MBEDTLS_ENTROPY_C) && \
    defined(MBEDTLS_CTR_DRBG_C)
#include "mbedtls/rsa.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"

#include <string.h>

#endif

#if !defined(MBEDTLS_BIGNUM_C) || !defined(MBEDTLS_RSA_C) ||  \
    !defined(MBEDTLS_FS_IO) || !defined(MBEDTLS_ENTROPY_C) || \
    !defined(MBEDTLS_CTR_DRBG_C)

#error "MBEDTLS_BIGNUM_C and/or MBEDTLS_RSA_C and/or MBEDTLS_FS_IO and/or MBEDTLS_ENTROPY_C and/or MBEDTLS_CTR_DRBG_C not defined."
#else


extern mbedtls_rsa_context rsa_context;
extern mbedtls_entropy_context rsa_entropy;
extern mbedtls_ctr_drbg_context rsa_ctr_drbg;
unsigned char rsa_result[1024];
unsigned char rsa_buff[513];

int rsa_module_init(char public_key_file[], 
		    mbedtls_rsa_context *rsa_context,
		    mbedtls_entropy_context *rsa_entropy,
		    mbedtls_ctr_drbg_context *rsa_ctr_drbg);

int rsa_decrypt(mbedtls_rsa_context *rsa_context,
		mbedtls_entropy_context *rsa_entropy,
		mbedtls_ctr_drbg_context *rsa_ctr_drbg,
		char *encrypted_message,
		unsigned char *rsa_result);

#endif
