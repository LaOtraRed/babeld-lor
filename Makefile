PREFIX = /usr/local
MANDIR = $(PREFIX)/share/man

CDEBUGFLAGS = -Os -g -Wall -Idecrypt-lib/
# the following results a smaller executable
#CDEBUGFLAGS = -Os -Wall -Idecrypt-lib/

DEFINES = $(PLATFORM_DEFINES)

CFLAGS = $(CDEBUGFLAGS) $(DEFINES) $(EXTRA_DEFINES)

LDLIBS = -lrt
LDLIBS += -D_FILE_OFFSET_BITS=64
LDLIBS += -Ldecrypt-lib
# dynamic libraries
LDLIBS += decrypt-lib/libmbedcrypto.so
LDLIBS += decrypt-lib/libmbedx509.so 
LDLIBS += decrypt-lib/libmbedtls.so 
# using static libraries:
#LDLIBS += decrypt-lib/libmbedcrypto.a
#LDLIBS += decrypt-lib/libmbedx509.a 
#LDLIBS += decrypt-lib/libmbedtls.a

SRCS = babeld.c net.c kernel.c util.c interface.c source.c neighbour.c \
       route.c xroute.c message.c resend.c configuration.c local.c \
       disambiguation.c rule.c \
	decrypt.c \
	lorauth.c

OBJS =  babeld.o net.o kernel.o util.o interface.o source.o neighbour.o \
       route.o xroute.o message.o resend.o configuration.o local.o \
       disambiguation.o rule.o \
	decrypt.o \
	lorauth.o

babeld: decrypt-lib $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o babeld $(OBJS) $(LDLIBS)

babeld.o: babeld.c version.h

local.o: local.c version.h

kernel.o: kernel_netlink.c kernel_socket.c

version.h:
	./generate-version.sh > version.h

# decrypt.o: decrypt.c
# 	@echo "-- decrypt utils"
# 	$(CC) -Idecrypt-lib/ -D_FILE_OFFSET_BITS=64 -O2 decrypt.c \
# 	-Ldecrypt-lib/ \
# 	decrypt-lib/libmbedcrypto.a \
# 	decrypt-lib/libmbedx509.a \
# 	decrypt-lib/libmbedtls.a

.SUFFIXES: .man .html

.man.html:
	mandoc -Thtml $< > $@

babeld.html: babeld.man

decrypt-lib: 
	@echo "Building decryption libraries..."
	$(MAKE) -C decrypt-lib all SHARED=True

decrypt_tests: decrypt-lib
	@echo "... building decrypt_tests (static)"
	$(CC) -o decrypt_tests -Idecrypt-lib/ -D_FILE_OFFSET_BITS=64 -O2 decrypt_tests.c -L./decrypt-lib ./decrypt-lib/libmbedcrypto.a decrypt-lib/libmbedx509.a decrypt-lib/libmbedtls.a


.PHONY: all install install.minimal uninstall clean decrypt-lib

all: babeld babeld.man decrypt-lib \
#decrypt_tests # pruebas de descifrado

install.minimal: babeld
	-rm -f $(TARGET)$(PREFIX)/bin/babeld
	mkdir -p $(TARGET)$(PREFIX)/bin
	cp -f babeld $(TARGET)$(PREFIX)/bin

install: install.minimal all
	mkdir -p $(TARGET)$(MANDIR)/man8
	cp -f babeld.man $(TARGET)$(MANDIR)/man8/babeld.8

uninstall:
	-rm -f $(TARGET)$(PREFIX)/bin/babeld
	-rm -f $(TARGET)$(MANDIR)/man8/babeld.8

clean:
#$(MAKE) -C decrypt-lib clean 
	-rm -f decrypt_tests
	-rm -f babeld babeld.html version.h *.o *~ core TAGS gmon.out 

distclean:
	$(MAKE) -C decrypt-lib clean
	-rm -f decrypt_tests
	-rm -f babeld babeld.html version.h *.o *~ core TAGS gmon.o
